angular.module("demo").controller("NestedListsDemoController", function($scope) {

    $scope.models = {
        selected: null,
        templates: [
            {type: "tag", name: "div", id: "", class: "" , columns: [[], []]},
        ],
        dropzones: {
          "elements": [
            {
              "type": "tag",
              "name": "header",
              "id": "",
              "class": "header",
              "columns": [
                [
                  {
                    "type": "tag",
                    "name": "div",
                    "id": "topPanel",
                    "class": "ecm-panel",
                    "columns": [
                      [],
                      []
                    ]
                  }
                ],
                []
              ]
            },
            {
              "type": "tag",
              "name": "main",
              "id": "",
              "class": "",
              "columns": [
                [
                  {
                    "type": "tag",
                    "name": "div",
                    "id": "",
                    "class": "page-content",
                    "columns": [
                      [
                        {
                          "type": "tag",
                          "name": "div",
                          "id": "banner",
                          "class": "ecm-panel",
                          "columns": [
                            [],
                            []
                          ]
                        }
                      ],
                      [
                        {
                          "type": "tag",
                          "name": "div",
                          "id": "",
                          "class": "row",
                          "columns": [
                            [
                              {
                                "type": "tag",
                                "name": "div",
                                "id": "",
                                "class": "col-md-9 col-12",
                                "columns": [
                                  [
                                    {
                                      "type": "tag",
                                      "name": "div",
                                      "id": "leftPanel",
                                      "class": "ecm-panel",
                                      "columns": [
                                        [],
                                        []
                                      ]
                                    }
                                  ],
                                  []
                                ]
                              },
                              {
                                "type": "tag",
                                "name": "div",
                                "id": "",
                                "class": "col-md-3 col-12",
                                "columns": [
                                  [
                                    {
                                      "type": "tag",
                                      "name": "div",
                                      "id": "rightPanel",
                                      "class": "ecm-panel",
                                      "columns": [
                                        [],
                                        []
                                      ]
                                    }
                                  ],
                                  []
                                ]
                              }
                            ],
                            []
                          ]
                        }
                      ]
                    ]
                  }
                ],
                []
              ]
            },
            {
              "type": "tag",
              "name": "footer",
              "id": "",
              "class": "",
              "columns": [
                [
                  {
                    "type": "tag",
                    "name": "div",
                    "id": "bottomPanel",
                    "class": "ecm-panel",
                    "columns": [
                      [],
                      []
                    ]
                  }
                ],
                []
              ]
            }
          ]
        }
    };

    $scope.$watch('models.dropzones', function(model) {
      $scope.htmlContent = "";
      for (let i = 0; i < $scope.models.dropzones.elements.length; i++) {
          const element = $scope.models.dropzones.elements[i];

          const tag = document.createElement(`${element.name}`);
          if (element.id !== "" && element.id !== undefined) {
            tag.id = element.id;
          }
          if (element.class !== "" && element.class !== undefined) {
            tag.className = element.class;
          }

          recursiveColumn(element.columns, tag);
          $scope.htmlContent += tag.outerHTML;
        };

        $scope.modelAsJson = angular.toJson(model, true);
    }, true);

    function recursiveColumn(arrayColumns, tag){
      for (let j = 0; j < arrayColumns.length; j++) {
        const arraySubColumns = arrayColumns[j];
        if (arraySubColumns !== undefined && arraySubColumns.length > 0) {
          for (let i = 0; i < arraySubColumns.length; i++) {
            const element = arraySubColumns[i];

            const subTag = document.createElement(`${element.name}`);
            if (element.id !== "" && element.id !== undefined) {
              subTag.id = element.id;
            }
            if (element.class !== "" && element.class !== undefined) {
              subTag.className = element.class;
            }
            
            tag.appendChild(subTag);

            recursiveColumn(element.columns, subTag);
          }
        }
      }
    }
});
